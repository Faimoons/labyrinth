package com.pvt.test;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Jiona
 * Date: 22.02.14
 * Time: 6:06
 * To change this template use File | Settings | File Templates.
 */
public class PathFinder {

    private final Labyrinth labyrinth;
    private final Point startPoint;
    private int[][] path;
    private Deque<Point> allPoint;
    private boolean[][] visited;
    private final int[] bottomTop = {-1, 0, 0, 1};
    private final int[] leftRight = {0, -1, 1, 0};
    private Point nextPointInDeque;
    private List<Point> list;

    public PathFinder(Labyrinth labyrinth, Point startPoint) {
        this.labyrinth = labyrinth;
        this.startPoint = startPoint;
    }

    /**
     * Finds path from start point to end point or return null if there is no path
     *
     * @param endPoint the end point
     * @return the path or null if path doesn't exists
     */


    public List<Point> findPath(Point endPoint) {
        this.path = new int[labyrinth.getHeight()][labyrinth.getWidth()];
        this.visited = new boolean[labyrinth.getHeight()][labyrinth.getWidth()];
        this.allPoint = new ArrayDeque<Point>();
        allPoint.add(startPoint);
        this.nextPointInDeque = allPoint.peekFirst();
        int x = nextPointInDeque.getI();
        int y = nextPointInDeque.getJ();
        path[x][y] = 1;
        while (!allPoint.isEmpty()) {
            nextPointInDeque = allPoint.pop();
            x = nextPointInDeque.getI();
            y = nextPointInDeque.getJ();
            visited[x][y] = true;
            if (endPoint.equals(nextPointInDeque)) {
                break;
            } else
                for (int i = 0; i < 4; i++) {
                    int row = x + bottomTop [i];
                    int cow = y + leftRight[i];
                    if (labyrinth.isRoomFinalized(row, cow) && !visited[row][cow]) {
                        allPoint.add(new Point(row, cow));
                        path[row][cow] = path[x][y] + 1;
                    }
                }
        }
        allPoint.clear();


        x = endPoint.getI();
        y = endPoint.getJ();
        this.list = new ArrayList<>(path[x][y]);
        if (path[x][y] != 0) {
            allPoint.add(endPoint);
            Deque<Point> g = new ArrayDeque<>();
            while (!allPoint.isEmpty()) {
                nextPointInDeque = allPoint.pop();
                x = nextPointInDeque.getI();
                y = nextPointInDeque.getJ();
                for (int i = 0; i < 4; i++) {
                    int row = x + bottomTop[i];
                    int cow = y + leftRight[i];
                    if (labyrinth.isRoomFinalized(row, cow) && (path[row][cow] == path[x][y] - 1)) {
                        allPoint.add(new Point(row, cow));
                        g.addFirst(nextPointInDeque);
                        if (path[row][cow] == 1) {
                            g.addFirst(startPoint);
                        }

                    }
                }
            }
            while (!g.isEmpty()){
               list.add(g.pollFirst());
            }
        }

        return list;
    }


    /**
     * Checks whether path from start point to end point exists
     *
     * @param endPoint the end point
     * @return flag is path exists or not
     */
    public boolean isPathExists(Point endPoint) {
        return (!findPath(endPoint).isEmpty());
    }
}
